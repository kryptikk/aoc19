package de.kryptikk.adventcode.advent2019.day12;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day12 {
	
	static List<Moon> moons = new ArrayList<>();
	static List<Moon> origMoons = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = reader.readLine()) != null) {
			line = line.replaceAll("<", "").replaceAll(">", "");
			String[] coords = line.split(", ");
			moons.add(new Moon(coords[0].split("=")[1], coords[1].split("=")[1], coords[2].split("=")[1]));
			origMoons.add(new Moon(coords[0].split("=")[1], coords[1].split("=")[1], coords[2].split("=")[1]));
		}
		
		long multi = 1;
		long[] seen = {0L,0L,0L};
		boolean allSeen = false;
		for (long i = 1; !allSeen; i++) {
			applyGravity();
			applyVelocity();
			//part 1
			if (i == 1000)
				computeEnergy();
			//part 2
			boolean[] same = {true, true, true};
			for (int j = 0; j < moons.size(); j++) {
				Moon m1 = moons.get(j);
				Moon m2 = origMoons.get(j);
				same[0] &= m1.x == m2.x && m1.velX == m2.velX;
				same[1] &= m1.y == m2.y && m1.velY == m2.velY;
				same[2] &= m1.z == m2.z && m1.velZ == m2.velZ;
			}
			for (int k = 0; k < same.length; k++) {
				if (seen[k] == 0L)
					seen[k] = same[k] ? i : 0L;
			}
			allSeen = seen[0] > 0 && seen[1] > 0 && seen[2] > 0;
		}
		System.out.println(lcm(seen[0], seen[1], seen[2]));
	}
	
	private static long lcm(long a, long b, long c) {
		return LCM(LCM(a, b), c);
	}

	private static long LCM(long a, long b) {
		return (a * b) / GCF(a, b);
	}

	private static long GCF(long a, long b) {
		if (b == 0)
			return a;
		else
			return (GCF(b, a%b));
	}

	private static void applyGravity() {
		for (int i = 0; i < moons.size(); i++) {
			Moon first = moons.get(i);
			for (int j = i+1; j < moons.size(); j++) {
				Moon second = moons.get(j);
				first.velX += first.x == second.x ? 0 : first.x < second.x ? 1 : -1;
				second.velX += first.x == second.x ? 0 : second.x < first.x ? 1 : -1;
				
				first.velY += first.y == second.y ? 0 : first.y < second.y ? 1 : -1;
				second.velY += first.y == second.y ? 0 : second.y < first.y ? 1 : -1;
				
				first.velZ += first.z == second.z ? 0 : first.z < second.z ? 1 : -1;
				second.velZ += first.z == second.z ? 0 : second.z < first.z ? 1 : -1;
			}
		}
	}

	private static void applyVelocity() {
		for (Moon m : moons) {
			m.x += m.velX;
			m.y += m.velY;
			m.z += m.velZ;
		}
	}
	
	private static void computeEnergy() {
		int total = 0;
		for (Moon m : moons) {
			m.pot = Math.abs(m.x) + Math.abs(m.y) + Math.abs(m.z);
			m.kin = Math.abs(m.velX) + Math.abs(m.velY) + Math.abs(m.velZ);
			m.tot = m.kin * m.pot;
			total += m.tot;
		}
		System.out.println(total);
	}

	private static class Moon {

		public int tot;
		public int kin;
		public int pot;
		private int x;
		private int y;
		private int z;
		
		private int velX = 0, velY = 0, velZ = 0;

		public Moon(String x, String y, String z) {
			this.x = Integer.parseInt(x);
			this.y = Integer.parseInt(y);
			this.z = Integer.parseInt(z);
		}
		
	}

}
