package de.kryptikk.adventcode.advent2019.day08;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Day8 {
	
	public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	public static String input = "";
	public static int width = 25;
	public static int height = 6;
	public static ArrayList<int[]> layers = new ArrayList<>();

	public static void main(String[] args) {
		input = sneak(() -> reader.readLine());
		readLayers();
		part1();
		part2();
	}
	
	private static void readLayers() {
		String layerString = "";
		int start = 0;
		int finish = width * height;
		while (start < input.length() && finish <= input.length()) {
			layerString = input.substring(start, finish);
			int[] layer = new int[width*height];
			for (int i = 0; i < layerString.length(); i++) {
				layer[i] = Integer.parseInt(layerString.substring(i, i+1));
			}
			layers.add(layer);
			start += width * height;
			finish += width * height;
		}
	}

	private static void part1() {
		int countZeros = Integer.MAX_VALUE;
		int solution = 0;
		int[] occur = {0,0,0};
		for (int[] layer : layers) {
			for (int i = 0; i < layer.length; i++) {
				occur[layer[i]]++;
			}
			if (occur[0] < countZeros) {
				countZeros = occur[0];
				solution = occur[1] * occur[2];
			}
			occur[0] = 0; occur[1] = 0; occur[2] = 0;
		}
		System.out.println(solution);
	}
	
	public static void part2() {
		int[] image = new int[width*height];
		for (int i = 0; i < image.length; i++) {
			image[i] = 2;
			for (int[] layer : layers) {
				if (image[i] == 2 && layer[i] != 2)
					image[i] = layer[i];
			}
		}
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int pixel = image[i+j*width];
				if (pixel == 2) {
					System.out.print(" ");
				}
				if (pixel == 1) {
					System.out.print("#");
				}
				if (pixel == 0) {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

}
