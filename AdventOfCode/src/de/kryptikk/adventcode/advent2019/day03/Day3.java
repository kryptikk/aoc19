package de.kryptikk.adventcode.advent2019.day03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Day3 {
	public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	
	public static int[] getDir(char c) {
		switch (c) {
		case 'U': return new int[] {0,1};
		case 'D': return new int[] {0,-1};
		case 'L': return new int[] {-1,0};
		case 'R': return new int[] {1,0};
		}
		return new int[] {0,0};
	}

	public static void main(String[] args) throws IOException {
		Map<String, Integer> wire = new HashMap<String, Integer>();
		String[] input = reader.readLine().split(",");
		
		int closest = Integer.MAX_VALUE;
		int shortest = Integer.MAX_VALUE;
		
		int x = 0, y = 0, d = 0;
		
		for (int i = 0; i < input.length; i++) {
			int[] dir = getDir(input[i].charAt(0));
			int len = Integer.parseInt(input[i].substring(1));
			for (int j = 0; j < len; j++) {
				int newX = x + dir[0];
				int newY = y + dir[1];
				wire.put(newX+"_"+newY, ++d);
				x = newX;
				y = newY;
			}
		}
		
		input = reader.readLine().split(",");
		x = y = d = 0;
		for (int i = 0; i < input.length; i++) {
			int[] dir = getDir(input[i].charAt(0));
			int len = Integer.parseInt(input[i].substring(1));
			for (int j = 0; j < len; j++) {
				int newX = x + dir[0];
				int newY = y + dir[1];
				d++;
				if (wire.containsKey(newX+"_"+newY)) {
					closest = Math.min(closest, Math.abs(newX) + Math.abs(newY));
					shortest = Math.min(shortest, wire.get(newX+"_"+newY)+d);
				}
				x = newX;
				y = newY;
			}
		}
		System.out.println(closest);
		System.out.println(shortest);
	}

}
