package de.kryptikk.adventcode.advent2019.day09;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

import de.kryptikk.adventcode.advent2019.intcomp.LongComputer;

public class Day9 {
	
	private String inputFile;

	public Day9(String string) {
		this.inputFile = string;
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		Day9 d = new Day9(args[0]);
		d.part1();
		d.part2();
	}

	private void part1() throws FileNotFoundException, IOException {
		BlockingQueue<Long> inputStream = new LinkedBlockingQueue<>();
		BlockingQueue<Long> outputStream = new LinkedBlockingQueue<>();
		Supplier<Long> input = sneaked(inputStream::take);
		Consumer<Long> output = sneaked(outputStream::put);
		LongComputer intComp = new LongComputer(input, output);
		intComp.readInput(inputFile);
		inputStream.offer(1L);
		intComp.execProgram();
		Long out = null;
		while ((out = outputStream.poll()) != null) {
			System.out.println(out);
		}
	}
	
	private void part2() throws FileNotFoundException, IOException {
		BlockingQueue<Long> inputStream = new LinkedBlockingQueue<>();
		BlockingQueue<Long> outputStream = new LinkedBlockingQueue<>();
		Supplier<Long> input = sneaked(inputStream::take);
		Consumer<Long> output = sneaked(outputStream::put);
		LongComputer intComp = new LongComputer(input, output);
		intComp.readInput(inputFile);
		inputStream.offer(2L);
		intComp.execProgram();
		Long out = null;
		while ((out = outputStream.poll()) != null) {
			System.out.println(out);
		}
	}

}
