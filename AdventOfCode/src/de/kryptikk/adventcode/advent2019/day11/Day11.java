package de.kryptikk.adventcode.advent2019.day11;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

import de.kryptikk.adventcode.advent2019.intcomp.LongComputer;

public class Day11 {

	private String inputFile;

	public Day11(String input) {
		this.inputFile = input;
	}
	
	public void part1() throws InterruptedException {
		BlockingQueue<Long> inputStream = new LinkedBlockingQueue<>();
		BlockingQueue<Long> outputStream = new LinkedBlockingQueue<>();
		Supplier<Long> input = sneaked(inputStream::take);
		Consumer<Long> output = sneaked(outputStream::put);
		LongComputer longComp = new LongComputer(input, output);
		boolean[] running = {true};
		try {
			longComp.readInput(inputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Thread t = new Thread(() -> {
			longComp.execProgram();
			running[0] = false;
		});
		t.start();
		Map<String, Long> grid = new HashMap<>();
		long curX = 0;
		long curY = 0;
		int[][] directions = {{0,1}, {-1,0}, {0,-1}, {1,0}};
		int curDir = 0;
		do {
			String pos = curX+"_"+curY;
			Long color = grid.get(pos);
			if (color == null)
				color = 0L;
			inputStream.offer(color);
			if (!running[0])
				continue;
			Long newColor = outputStream.take();
			grid.put(pos, newColor);
			Long dirDelta = outputStream.take();
			if (dirDelta == 1)
				curDir = (curDir+1);
			else
				curDir = (curDir-1);
			if (curDir > 3)
				curDir = 0;
			if (curDir < 0)
				curDir = 3;
			curX += directions[curDir][0];
			curY += directions[curDir][1];
		} while(running[0]);
		System.out.println(grid.entrySet().size());
	}
	
	public void part2() throws InterruptedException {
		BlockingQueue<Long> inputStream = new LinkedBlockingQueue<>();
		BlockingQueue<Long> outputStream = new LinkedBlockingQueue<>();
		Supplier<Long> input = sneaked(inputStream::take);
		Consumer<Long> output = sneaked(outputStream::put);
		LongComputer longComp = new LongComputer(input, output);
		boolean[] running = {true};
		try {
			longComp.readInput(inputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Thread t = new Thread(() -> {
			longComp.execProgram();
			running[0] = false;
		});
		t.start();
		Map<String, Long> grid = new HashMap<>();
		grid.put("0_0", 1L);
		long curX = 0;
		long curY = 0;
		long minX = 0L, maxX = 0L, minY = 0L, maxY = 0L;
		int[][] directions = {{0,1}, {-1,0}, {0,-1}, {1,0}};
		int curDir = 0;
		do {
			if (curX < minX)
				minX = curX;
			if (curX > maxX)
				maxX = curX;
			if (curY < minY)
				minY = curY;
			if (curY > maxY)
				maxY = curY;
			String pos = curX+"_"+curY;
			Long color = grid.get(pos);
			if (color == null)
				color = 0L;
			inputStream.offer(color);
			if (!running[0])
				continue;
			Long newColor = outputStream.take();
			grid.put(pos, newColor);
			if (!running[0])
				continue;
			Long dirDelta = outputStream.take();
			if (dirDelta == 1)
				curDir = (curDir+1);
			else
				curDir = (curDir-1);
			if (curDir > 3)
				curDir = 0;
			if (curDir < 0)
				curDir = 3;
			curX += directions[curDir][0];
			curY += directions[curDir][1];
		} while(running[0]);
		for (long y = maxY+2; y >= minY-2; y--) {
			for (long x = maxX+2; x >= minX-2; x--) {
				Long color = grid.get(x+"_"+y);
				if (color == null)
					color = 0L;
				if (color == 0L)
					System.out.print(".");
				else
					System.out.print("#");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		Day11 d = new Day11(args[0]);
		d.part1();
		d.part2();
	}

}
