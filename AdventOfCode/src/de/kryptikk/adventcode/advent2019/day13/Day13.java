package de.kryptikk.adventcode.advent2019.day13;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

import de.kryptikk.adventcode.advent2019.intcomp.LongComputer;

public class Day13 {

	private String inputFile;

	public Day13(String inputFile) {
		this.inputFile = inputFile;
	}
	
	private void part1() {
		BlockingQueue<Long> inputStream = new LinkedBlockingQueue<>();
		BlockingQueue<Long> outputStream = new LinkedBlockingQueue<>();
		Supplier<Long> input = sneaked(inputStream::take);
		Consumer<Long> output = sneaked(outputStream::put);
		LongComputer longComp = new LongComputer(input, output);
		try {
			longComp.readInput(inputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		longComp.execProgram();
		
		int counter = 0;
		long x = 0, y = 0;
		Map<Long, Long> tiles = new HashMap<>();
		while (outputStream.peek() != null) {
			Long out = outputStream.remove();
			switch (counter) {
			case 0: x = out; break;
			case 1: y = out; break;
			case 2: setTile(tiles, x, y, out); break;
			case 3: x = out; break;
			case 4: y = out; break;
			case 5: setTile(tiles, x, y, out); break;
			}
			counter = (counter+1) % 6;
		}
		
		long answer1 = tiles.values().stream().filter(t -> t == 2L).count();
		System.out.println(answer1 + " block tiles");
	}
	
	private void setTile(Map<Long, Long> tiles, long x, long y, long tile) {
		long coords = x*100000+y;
		tiles.put(coords, tile);
	}

	private void part2() {
		
	}

	public static void main(String[] args) {
		Day13 d = new Day13(args[0]);
		d.part1();
		d.part2();
	}

}
