package de.kryptikk.adventcode.advent2019.intcomp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class IntComputer {

	ArrayList<Long> program = new ArrayList<>();
	private final Supplier<Long> input;
	private final Consumer<Long> output;
	private long relBase = 0;
	
	public IntComputer(Supplier<Long> input, Consumer<Long> output) {
		this.input = input;
		this.output = output;
	}

	public void execProgram() {
		long pc = 0;
		while (true) {
			Instruction ins = Instruction.of(program, pc, relBase);
//			ins.print();
			long param1 = ins.param1;
			long param2 = ins.param2;
			long param3 = ins.param3;
			switch(ins.opCode) {
			case ADD: {
				long pos = program.get((int) (pc+3));
				if (program.size() <= pos) {
					while (program.size() <= pos)
						program.add(0L);
				}
				program.set((int) pos, param1+param2);
				pc+=4;
				break;
			}
			case MULT: {
				long pos = program.get((int) (pc+3));
				if (program.size() <= pos) {
					while (program.size() <= pos)
						program.add(0L);
				}
				program.set((int) pos, param1*param2);
				pc+=4;
				break;
			}
			case INPUT: {
				long pos = param1;
				if (program.size() <= pos) {
					while (program.size() <= pos)
						program.add(0L);
				}
				program.set((int) pos, input.get());
				pc+=2;
				break;
			}
			case OUTPUT: {
				output.accept(param1);
				pc+=2;
				break;
			}
			case JUMP_IF_TRUE: {
				pc = param1 != 0 ? param2 : pc+3;
				break;
			}
			case JUMP_IF_FALSE: {
				pc = param1 == 0 ? param2 : pc+3;
				break;
			}
			case LESS_THAN: {
				long pos = program.get((int) (pc+3));
				if (program.size() <= pos) {
					while (program.size() <= pos)
						program.add(0L);
				}
				program.set((int) pos, param1 < param2 ? 1L : 0L);
				pc += 4;
				break;
			}
			case EQUALS: {
				long pos = program.get((int) (pc + 3));
				if (program.size() <= pos) {
					while (program.size() <= pos)
						program.add(0L);
				}
				program.set((int) pos, param1 == param2 ? 1L : 0L);
				pc += 4;
				break;
			}
			case ADJUST_REL_BASE: {
				relBase += param1;
				pc += 2;
				break;
			}
			case HALT: {
				return;
			}
			}
		}
	}

	public void readInput(String inputFile) throws FileNotFoundException, IOException {
		program.clear();
		try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
			String programLine = reader.readLine();
			String[] programArgs = programLine.split(",");
			for (String arg : programArgs) {
				program.add(Long.parseLong(arg));
			}
			
		}
	}
	
	private static class Instruction {
		private final OpCode opCode;
		private final Long param1;
		private final Long param2;
		private final Long param3;
		
		private Instruction(OpCode opCode, Long param1, Long param2, Long param3) {
			this.opCode = opCode;
			this.param1 = param1;
			this.param2 = param2;
			this.param3 = param3;
		}
		
		private static Instruction of(List<Long> program, long pc, long relBase) {
			long code = program.get((int) pc);
//			if (code == 203) {
//				System.out.println();
//			}
			OpCode opCode = OpCode.of(code % 100);
			long param1 = getParam(program, Mode.of((code / 100) % 10), pc+1, relBase);
			long param2 = getParam(program, Mode.of((code / 1000) % 10), pc+2, relBase);
			long param3 = getParam(program, Mode.of((code / 10000) % 10), pc+3, relBase);
			return new Instruction(opCode, param1, param2, param3);
		}
		
		private static Long getParam(List<Long> program, Mode mode, long offset, long relBase) {
			long ref = program.get((int) offset);
			switch (mode) {
			case IMMEDIATE: return ref;
			case POSITION: {
				long pos = ref;
				if (program.size() <= pos) {
					while (program.size() <= pos)
						program.add(0L);
				}
				return pos < program.size() && pos >= 0 ? program.get((int) pos) : 0;
			}
			case RELATIVE: {
				long pos = ref+relBase;
				if (program.size() <= pos) {
					while (program.size() <= pos)
						program.add(0L);
				}
				return (pos) >= 0 ? program.get((int) (pos)) : 0;
			}
			}
			return null;
		}
		private void print() {
			switch (this.opCode) {
			case ADD: System.out.println("ADD");
				break;
			case ADJUST_REL_BASE: System.out.println("ADJUST");
				break;
			case EQUALS: System.out.println("EQUALS");
				break;
			case HALT: System.out.println("HALT");
				break;
			case INPUT: System.out.println("INPUT");
				break;
			case JUMP_IF_FALSE: System.out.println("JUMP_FALSE");
				break;
			case JUMP_IF_TRUE: System.out.println("JUMP_TRUE");
				break;
			case LESS_THAN: System.out.println("LESS");
				break;
			case MULT: System.out.println("MULT");
				break;
			case OUTPUT: System.out.println("OUTPUT");
				break;
			default:
				break;
			}
			System.out.println();
		}
	}
	
	
	
	private enum Mode {
		POSITION,
		IMMEDIATE,
		RELATIVE;
		
		private static Mode of(long code) {
			switch ((int) code) {
			case 0: return POSITION;
			case 1: return IMMEDIATE;
			case 2: return RELATIVE;
			default: return POSITION;
			}
		}
	}
	
	private enum OpCode {
		ADD,
		MULT,
		INPUT,
		OUTPUT,
		JUMP_IF_TRUE,
		JUMP_IF_FALSE,
		LESS_THAN,
		EQUALS,
		ADJUST_REL_BASE,
		HALT;
		
		private static OpCode of(long code) {
			switch ((int) code) {
			case 1: return ADD;
			case 2: return MULT;
			case 3: return INPUT;
			case 4: return OUTPUT;
			case 5: return JUMP_IF_TRUE;
			case 6: return JUMP_IF_FALSE;
			case 7: return LESS_THAN;
			case 8: return EQUALS;
			case 9: return ADJUST_REL_BASE;
			case 99: return HALT;
			default: return HALT;
			}
		}
	}
}
