package de.kryptikk.adventcode.advent2019.intcomp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class LongComputer {

	ArrayList<Long> program = new ArrayList<>();
	private final Supplier<Long> input;
	private final Consumer<Long> output;
	private long relBase = 0;
	
	public LongComputer(Supplier<Long> input, Consumer<Long> output) {
		this.input = input;
		this.output = output;
	}

	public void execProgram() {
		long pc = 0;
		while (true) {
			long inst = program.get((int) pc);
			OpCode opCode = OpCode.of(inst % 100);
			inst /= 100;
			Mode mod1 = Mode.of(inst % 10); inst /= 10;
			Mode mod2 = Mode.of(inst % 10); inst /= 10;
			Mode mod3 = Mode.of(inst % 10);
			switch(opCode) {
			case ADD: {
				long param1 = readValue(pc+1, mod1);
				long param2 = readValue(pc+2, mod2);
				writeValue(pc+3, mod3, param1+param2);
				pc+=4;
				break;
			}
			case MULT: {
				long param1 = readValue(pc+1, mod1);
				long param2 = readValue(pc+2, mod2);
				writeValue(pc+3, mod3, param1*param2);
				pc+=4;
				break;
			}
			case INPUT: {
				writeValue(pc+1, mod1, input.get());
				pc+=2;
				break;
			}
			case OUTPUT: {
				long param = readValue(pc+1, mod1);
				output.accept(param);
				pc+=2;
				break;
			}
			case JUMP_IF_TRUE: {
				long param1 = readValue(pc+1, mod1);
				pc = param1 != 0 ? readValue(pc+2, mod2) : pc+3;
				break;
			}
			case JUMP_IF_FALSE: {
				long param1 = readValue(pc+1, mod1);
				pc = param1 == 0 ? readValue(pc+2, mod2) : pc+3;
				break;
			}
			case LESS_THAN: {
				long param1 = readValue(pc+1, mod1);
				long param2 = readValue(pc+2, mod2);
				writeValue(pc+3, mod3, param1 < param2 ? 1 : 0);
				pc += 4;
				break;
			}
			case EQUALS: {
				long param1 = readValue(pc+1, mod1);
				long param2 = readValue(pc+2, mod2);
				writeValue(pc+3, mod3, param1 == param2 ? 1 : 0);
				pc += 4;
				break;
			}
			case ADJUST_REL_BASE: {
				long param1 = readValue(pc+1, mod1);
				relBase += param1;
				pc += 2;
				break;
			}
			case HALT: {
				return;
			}
			}
		}
	}

	public void readInput(String inputFile) throws FileNotFoundException, IOException {
		program.clear();
		try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
			String programLine = reader.readLine();
			String[] programArgs = programLine.split(",");
			for (String arg : programArgs) {
				program.add(Long.parseLong(arg));
			}
			
		}
	}
	
	private long readValue(long pos, Mode mode) {
		long off = -1;
		switch (mode) {
		case POSITION: off = program.get((int) pos); break;
		case IMMEDIATE: off = pos; break;
		case RELATIVE: off = relBase + program.get((int) pos); break;
		}
		return readMemory(off);
	}
	
	private long readMemory(long off) {
		if (off < 0)
			throw new RuntimeException("index out of bounds");
		if (off >= program.size())
			while (off >= program.size())
				program.add(0L);
		return program.get((int) off);
	}

	private void writeValue(long pos, Mode mode, long value) {
		long off = -1;
		switch (mode) {
		case POSITION: off = program.get((int) pos); break;
		case IMMEDIATE: throw new RuntimeException("cannot use immediate mode while writing");
		case RELATIVE: off = relBase + program.get((int) pos); break;
		}
		
		writeMemory(off, value);
	}
	
	private void writeMemory(long off, long value) {
		if (off < 0)
			throw new RuntimeException("index out of bounds");
		if (off >= program.size())
			while (off >= program.size())
				program.add(0L);
		program.set((int) off, value);
	}

	private enum Mode {
		POSITION,
		IMMEDIATE,
		RELATIVE;
		
		private static Mode of(long code) {
			switch ((int) code) {
			case 0: return POSITION;
			case 1: return IMMEDIATE;
			case 2: return RELATIVE;
			default: return POSITION;
			}
		}
	}
	
	private enum OpCode {
		ADD,
		MULT,
		INPUT,
		OUTPUT,
		JUMP_IF_TRUE,
		JUMP_IF_FALSE,
		LESS_THAN,
		EQUALS,
		ADJUST_REL_BASE,
		HALT;
		
		private static OpCode of(long code) {
			switch ((int) code) {
			case 1: return ADD;
			case 2: return MULT;
			case 3: return INPUT;
			case 4: return OUTPUT;
			case 5: return JUMP_IF_TRUE;
			case 6: return JUMP_IF_FALSE;
			case 7: return LESS_THAN;
			case 8: return EQUALS;
			case 9: return ADJUST_REL_BASE;
			case 99: return HALT;
			default: return HALT;
			}
		}
	}
}
