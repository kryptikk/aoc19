package de.kryptikk.adventcode.advent2019.day07;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import com.rainerhahnekamp.sneakythrow.functional.SneakyConsumer;

import de.kryptikk.adventcode.advent2019.intcomp.IntComputer;

public class Day7 {
	static ArrayList<ArrayList<Long>> listOfPermutations = new ArrayList<>();
	private String inputFile;

	public Day7(String string) {
		this.inputFile = string;
	}

	private static void permute(List<Long> arr, int k) {
		for (int i = k; i < arr.size(); i++) {
			Collections.swap(arr, i, k);
			permute(arr, k+1);
			Collections.swap(arr, k, i);
		}
		if (k == arr.size()-1) {
			ArrayList<Long> newList = new ArrayList<>();
			newList.addAll(arr);
			listOfPermutations.add(newList);
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Day7 d = new Day7(args[0]);
		d.part1();
		listOfPermutations.clear();
		d.part2();
	}
	
	private void part1() throws FileNotFoundException, IOException {
		permute(Arrays.asList(0L,1L,2L,3L,4L), 0);
		long maxSignal = listOfPermutations
				.stream()
				.mapToLong(this::runAmplifiers)
				.max()
				.orElseThrow(() -> new IllegalStateException("No returns"));
		System.out.println(maxSignal);
	}
	
	private void part2() throws FileNotFoundException, IOException {
		permute(Arrays.asList(5L,6L,7L,8L,9L), 0);
		long maxSignal = listOfPermutations
				.stream()
				.mapToLong(this::runAmplifiers)
				.max()
				.orElseThrow(() -> new IllegalStateException("No Returns"));
		System.out.println(maxSignal);
	}
	
	private Long runAmplifiers(List<Long> phaseSettings) {
		List<LinkedBlockingQueue<Long>> streams = Stream.generate((Supplier<LinkedBlockingQueue<Long>>) LinkedBlockingQueue::new)
		        .limit(phaseSettings.size())
		        .collect(Collectors.toList());
		List<Thread> amplifiers = LongStream.range(0, phaseSettings.size())
				.mapToObj(i -> {
					Long phaseSetting = phaseSettings.get((int) i);
					LinkedBlockingQueue<Long> input = streams.get((int) i);
					LinkedBlockingQueue<Long> output = streams.get((int) ((i+1) % phaseSettings.size()));
					return runAmplifier(phaseSetting, input, output);
				})
				.collect(Collectors.toList());
		streams.get(0).add((long) 0);
		amplifiers.forEach(sneaked((SneakyConsumer<Thread, Exception>) Thread::join));
		return streams.get(0).remove();
	}

	private Thread runAmplifier(Long phaseSetting, BlockingQueue<Long> inputStream, BlockingQueue<Long> outputStream) {
		sneaked(() -> inputStream.put(phaseSetting)).run();
		Thread thread = new Thread(() -> {
			Supplier<Long> input = sneaked(inputStream::take);
			Consumer<Long> output = sneaked(outputStream::put);
			IntComputer intComp = new IntComputer(input, output);
			try {
				intComp.readInput(inputFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			intComp.execProgram();
		});
		thread.start();
		return thread;
	}
	
}
