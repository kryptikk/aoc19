package de.kryptikk.adventcode.advent2019.day02;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Day2 {
	
	ArrayList<Long> program = new ArrayList<>();

	public void execProgram() {
		int pos = 0;
		long arg = program.get(0);
		while (arg != 99) {
			execStep(pos);
			pos += 4;
			arg = program.get(pos);
		}
	}

	private void execStep(int pos) {
		long cmd = program.get(pos);
		long ld1 = program.get(pos+1);
		long ld2 = program.get(pos+2);
		long str = program.get(pos+3);
		long nmbr1 = program.get((int) ld1);
		long nmbr2 = program.get((int) ld2);
		
		if (cmd == 1) {
			program.set((int) str, nmbr1+nmbr2);
		}
		if (cmd == 2) {
			program.set((int) str, nmbr1*nmbr2);
		}
	}

	private void readInput(String inputFile) throws FileNotFoundException, IOException {
		program.clear();
		try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
			String programLine = reader.readLine();
			String[] programArgs = programLine.split(",");
			for (String arg : programArgs) {
				program.add(Long.parseLong(arg));
			}
			
		}
	}
	
	public void setInput(long noun, long verb) {
		program.set(1, noun);
		program.set(2, verb);
	}
	
	public long getOutput() {
		return program.get(0);
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		Day2 solution = new Day2();
		for(long noun = 0; noun < 100; noun++) {
			for (long verb = 0; verb < 100; verb++) {
				solution.readInput(args[0]);
				solution.setInput(noun, verb);
				solution.execProgram();
				if (19690720 == solution.getOutput()) {
					System.out.println(100*noun + verb);
				}
			}
		}
	}

}
