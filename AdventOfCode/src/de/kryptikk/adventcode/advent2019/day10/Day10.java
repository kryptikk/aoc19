package de.kryptikk.adventcode.advent2019.day10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day10 {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(args[0]));
		ArrayList<Asteroid> asteroids = new ArrayList<>();
		String line = "";
		int y = 0;
		while ((line = reader.readLine()) != null) {
			for(int x = 0; x < line.length(); x++) {
				if (line.charAt(x) == '#')
					asteroids.add(new Asteroid(x,y));
			}
			y++;
		}
		reader.close();
		
		Comparator<Asteroid> cmp = Comparator.comparing(coords -> asteroids.stream()
				.filter(it -> it != coords)
				.mapToDouble(it -> Math.atan2((double)(it.y-coords.y), (double)(it.x-coords.x)))
				.distinct()
				.count());
		
		Long answer1 = asteroids.stream()
			.map(coords -> asteroids.stream()
					.filter(it -> it != coords)
					.mapToDouble(it -> Math.atan2((double)(it.y-coords.y), (double)(it.x-coords.x)))
					.distinct()
					.count())
			.max(Long::compare).get();
		
		System.out.println(answer1);
		
		Asteroid st = asteroids.stream().max(cmp).get();
		System.out.println("Station is at " + st.x + ":" + st.y);
		
		List<Asteroid> remaining = asteroids.stream().filter(it -> it != st).collect(Collectors.toList());
		double[] angle = {-Math.PI/2};
		boolean[] firstTarget = {true};
		int i = 1;
		while (i <= 200) {
			Map<Double, List<Asteroid>> asteroidsByAngle = remaining.stream().collect(Collectors.groupingBy(it -> Math.atan2((double)(it.y-st.y), (double)(it.x-st.x))));
			Map<Double, List<Asteroid>> inBetween = asteroidsByAngle.entrySet().stream()
				.filter(it -> {
					if (firstTarget[0]) 
						return (double)it.getKey() >= angle[0];
					else 
						return (double)it.getKey() > angle[0];
					}).collect(Collectors.toMap(e->e.getKey(), e->e.getValue()));
			Optional<Entry<Double, List<Asteroid>>> nextAngleTargetsOptional = inBetween.entrySet().stream()
				.min(Comparator.comparing(it ->it.getKey()));
			Entry<Double, List<Asteroid>> nextAngleTargets;
			if (nextAngleTargetsOptional.isPresent()) {
				nextAngleTargets = nextAngleTargetsOptional.get();
			} else {
				nextAngleTargets = asteroidsByAngle.entrySet().stream()
						.min(Comparator.comparing(it -> it.getKey())).get();
			}
			angle[0] = nextAngleTargets.getKey();
			firstTarget[0] = false;
			
			Stream<Asteroid> targetStream = nextAngleTargets.getValue().stream();
			Asteroid target = targetStream.min(Comparator.comparing(it -> Math.hypot(it.x-st.x, it.y-st.y))).get();
			remaining.remove(target);
			
			if (i == 200)
				System.out.println("Target " + i + " at " + target.x + ":" + target.y);
			i++;
		}
	}
	
	private static class Asteroid {
		int x = -1;
		int y = -1;
		
		public Asteroid(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

}
