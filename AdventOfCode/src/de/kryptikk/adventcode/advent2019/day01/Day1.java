package de.kryptikk.adventcode.advent2019.day01;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Day1 {
	
	private static long fuelSum = 0;
	private static long fuelSumWithAdditionalMass = 0;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		star1();
		System.out.println(fuelSum);
		System.out.println(fuelSumWithAdditionalMass);
	}

	private static void star1() throws FileNotFoundException, IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader("C:\\workspaces\\larsWorkspace\\AdventOfCode\\src\\de\\kryptikk\\adventcode\\advent2019\\day1\\input.txt"))) {
			String line;
			while ((line = reader.readLine()) != null) {
				long mass = Long.parseLong(line);
				fuelSum += fuelRequired(mass);
				fuelSumWithAdditionalMass += fuelRequired(mass) + calcAdditionalFuelRequired(fuelRequired(mass));
			}
		}
	}
	
	private static long calcAdditionalFuelRequired(long fuelRequired) {
		long additional = fuelRequired(fuelRequired);
		if (additional < 0) 
			return 0;
		long additionalSum = additional;
		while (additional > 0) {
			additional = fuelRequired(additional);
			if (additional > 0)
				additionalSum += additional;
		}
		return additionalSum;
	}

	private static long fuelRequired(long mass) {
		return mass / 3 - 2;
	}

}
