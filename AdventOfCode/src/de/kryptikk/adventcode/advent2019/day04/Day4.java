package de.kryptikk.adventcode.advent2019.day04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.IntStream;

public class Day4 {
	public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	
	public static void main(String[] args) throws Exception {
        String input = reader.readLine();
        Day4 day = new Day4();
        day.part1(input);
        day.part2(input);
    }

    public void part1(String input) {
        String[] in = input.split("-");
        int start = Integer.parseInt(in[0]);
        int end = Integer.parseInt(in[1]);

        long candidates = IntStream.rangeClosed(start, end) //
                .parallel() //
                .mapToObj(Integer::toString) //
                .map(String::toCharArray) //
                .filter(this::isCandidatePassword) //
                .count();

        System.out.printf("There are %d candidates.%n", candidates);
    }

    private boolean isCandidatePassword(char[] candidate) {
        boolean sawPair = false;
        for (int i = 1; i < candidate.length; ++i) {
            char prev = candidate[i - 1];
            char curr = candidate[i];
            if (prev > curr) {
                return false;
            }
            if (prev == curr) {
                sawPair = true;
            }
        }
        return sawPair;
    }

    public void part2(String input) {
        String[] in = input.split("-");
        int start = Integer.parseInt(in[0]);
        int end = Integer.parseInt(in[1]);

        long candidates = IntStream.rangeClosed(start, end) //
                .parallel() //
                .mapToObj(Integer::toString) //
                .map(String::toCharArray) //
                .filter(this::isCandidatePasswordWithMoreInformation) //
                .count();

        System.out.printf("There are %d candidates.%n", candidates);
    }

    private boolean isCandidatePasswordWithMoreInformation(char[] candidate) {
        int[] pairOccurrences = new int[10];
        for (int i = 1; i < candidate.length; ++i) {
            char prev = candidate[i - 1];
            char curr = candidate[i];
            if (prev > curr) {
                return false;
            }
            if (prev == curr) {
                ++pairOccurrences[prev - 0x30];
            }
        }

        for(int occurrences : pairOccurrences) {
            if(occurrences == 1) {
                return true;
            }
        }

        return false;
    }


}
