package de.kryptikk.adventcode.advent2019.day06;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Day6 {
	
	static int count = 0;
	static int wayCount = 0;

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(args[0]));
		HashMap<String, SpaceObject> spaceObjs = new HashMap<>();
		String inputLine = null;
		while ((inputLine = reader.readLine()) != null) {
			String[] names = inputLine.split("\\)");
			spaceObjs.computeIfAbsent(names[0], k -> new SpaceObject(k));
			spaceObjs.computeIfAbsent(names[1], k -> new SpaceObject(k));
			spaceObjs.get(names[0]).addOrbiter(spaceObjs.get(names[1]));
			spaceObjs.get(names[1]).orbits(spaceObjs.get(names[0]));
		}
		
		//part 1
		countOrbits(spaceObjs);
		
		//part 2
		SpaceObject start = spaceObjs.get("YOU").orbits;
		SpaceObject finish = spaceObjs.get("SAN").orbits;
		HashMap<SpaceObject, SpaceObject> pre = findRoute(spaceObjs, start, finish);
		SpaceObject curr = finish;
		while (!curr.equals(start)) {
			wayCount++;
			curr = pre.get(curr);
		}
		System.out.println(wayCount);
	}
	
	private static void countOrbits(HashMap<String, SpaceObject> spaceObjs) {
		SpaceObject start = spaceObjs.get("COM");
		for (SpaceObject obj : spaceObjs.values()) {
			SpaceObject curr = obj;
			HashMap<SpaceObject, SpaceObject> pre = findRoute(spaceObjs, start, curr);
			while (!curr.equals(start)) {
				count++;
				curr = pre.get(curr);
			}
		}
		System.out.println(count);
	}
	
	//Dijkstra
	private static HashMap<SpaceObject, SpaceObject> findRoute(HashMap<String, SpaceObject> graph, SpaceObject start, SpaceObject finish) {
		HashMap<SpaceObject, Integer> distance = new HashMap<>();
		HashMap<SpaceObject, SpaceObject> pre = new HashMap<>();
		Set<SpaceObject> Q = new HashSet<>();
		init(graph, start, distance, pre, Q);
		while(!Q.isEmpty()) {
			SpaceObject u = getMinDistance(Q, distance);
			Q.remove(u);
			if (u.equals(finish))
				return pre;
			for (SpaceObject v : u.orbiter) {
				if (Q.contains(v))
					distanceUpdate(u, v, distance, pre);
			}
			if (u.orbits != null && Q.contains(u.orbits))
				distanceUpdate(u, u.orbits, distance, pre);
		}
		return pre;
	}

	private static void distanceUpdate(SpaceObject u, SpaceObject v, HashMap<SpaceObject, Integer> distance,
			HashMap<SpaceObject, SpaceObject> pre) {
		int alternative = distance.get(u) + 1;
		if (alternative < distance.get(v)) {
			distance.put(v, alternative);
			pre.put(v, u);
		}
	}

	private static SpaceObject getMinDistance(Set<SpaceObject> q, HashMap<SpaceObject, Integer> distance) {
		Integer min = Integer.MAX_VALUE;
		SpaceObject ret = null;
		for (SpaceObject curr : q) {
			if (distance.get(curr) < min) {
				min = distance.get(curr);
				ret = curr;
			}
		}
		return ret;
	}

	private static void init(HashMap<String, SpaceObject> graph, SpaceObject start,
			HashMap<SpaceObject, Integer> distance, HashMap<SpaceObject, SpaceObject> pre, Set<SpaceObject> q) {
		for (SpaceObject obj : graph.values()) {
			distance.put(obj, Integer.MAX_VALUE);
		}
		distance.put(start, 0);
		q.addAll(graph.values());
	}

	private static class SpaceObject {

		private String name;
		private ArrayList<SpaceObject> orbiter = new ArrayList<>();
		private SpaceObject orbits = null;

		public SpaceObject(String k) {
			this.name = k;
		}
		
		public void addOrbiter(SpaceObject orbiter) {
			this.orbiter.add(orbiter);
		}
		
		public void orbits(SpaceObject orbits) {
			this.orbits = orbits;
		}
		
	}

}
